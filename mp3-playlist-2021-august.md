Snapshot of the files (excludng podcasts) on my mp3 player as of 19 August 2021:

```
.
├── A Frames - 04 - Death Train.lnk.mp3
├── A Frames - 06 - Eva Braun.lnk.mp3
├── A Frames - 07 - Black Forest II.lnk.mp3
├── A Million Billion Dying Suns - Do What You Do When The Time Is Right.mp3
├── A Place To Bury Strangers - In Your Heart.lnk.mp3
├── A Sunny Day In Glasgow - Daytime Rainbows.lnk.mp3
├── Airiel - Mermaid In A Manhole.lnk.mp3
├── Alvvays - In Undertow.mp3
├── Amon Tobin - Easy Muffin.mp3
├── Annie Hart - Hard To Be Still.mp3
├── Autechre - Slip.lnk.mp3
├── BCBG - White Trees (Opale remix).mp3
├── BCBG - White Trees.mp3
├── BEAK - Yatton.mp3
├── Band of Susans - Blind.mp3
├── Band of Susans - Hell Bent.lnk.mp3
├── Band of Susans - Ice Age.mp3
├── Band of Susans - Now is Now.mp3
├── Battles - Atlas.lnk.mp3
├── Be Your Own Pet - 12 - October, First Account.lnk.mp3
├── Beach Fossils - Birthday.lnk.mp3
├── Bear in Heaven - 01 - Beast In Peace.lnk.mp3
├── Belltower, The - 1 - Underwatertown.mp3
├── Bent Shapes - Panel of Experts.lnk.mp3
├── Best Coast - 02 - Crazy For You.lnk.mp3
├── Black Cab - 01 - Hearts On Fire.lnk.mp3
├── Black Cab - 06 - Surrender.lnk.mp3
├── Black Hearted Brother - (I Dont Mean To) Wonder.lnk.mp3
├── Black Rebel Motorcycle Club - 12 - Heart + Soul.lnk.mp3
├── Black Tambourine - 06 - Throw Aggi Off the Bridge.lnk.mp3
├── Black Tie Dynasty - Bells.lnk.mp3
├── Blaze, The - Territory.mp3
├── Blonde Redhead - 01 - 23.lnk.mp3
├── Blood Red Shoes - 09 - It's Getting Boring By The Sea.lnk.mp3
├── Blouse - Arrested.lnk.mp3
├── Blues Company - She's Gone.mp3
├── Boards of Canada - 02 - Chromakey Dreamcoat.mp3
├── Boards of Canada - 06 - Sunshine Recorder.mp3
├── Boards of Canada - 07 - '84 Pontiac Dream.mp3
├── Boards of Canada - 08 - Julie and Candy.mp3
├── Boards of Canada - 10 - 1969.mp3
├── Boards of Canada - 12 - The Beach at Redpoint.mp3
├── Boards of Canada - 14 - Alpha and Omega.mp3
├── Boards of Canada - 19 - Dawn Chorus.mp3
├── Boards of Canada - Everything You Do Is A Balloon.mp3
├── Boards of Canada - In A Beautiful Place Out In The Country.mp3
├── Boards of Canada - Kid For Today.mp3
├── Boards of Canada - Oirectine.mp3
├── Boards of Canada - skyliner.mp3
├── Bombay Bicycle Club - Your Eyes.lnk.mp3
├── Bonobo - Pick Up.mp3
├── Bossk - The Reverie.mp3
├── Bottom Of The Hudson - 09 - Hide And Seek.lnk.mp3
├── Brakes - 08 - I Can't Stand To Stand Beside You.lnk.mp3
├── Brakes - 10 - You're So Pretty.lnk.mp3
├── Broadcast - 02 - Pendulum.lnk.mp3
├── Broadcast - Corporeal.mp3
├── CYMBALS - Erosion.lnk.mp3
├── Can - Mother Sky.mp3
├── Caseworker, The - Boats.lnk.mp3
├── Cassis Orange - Still No Home.lnk.mp3
├── Catherine Wheel - 03 - Crank.lnk.mp3
├── Cedars - This Century.lnk.mp3
├── Chambermaids, The - China Blue.lnk.mp3
├── Chameleons (UK), The - 02 - Perfume Garden.lnk.mp3
├── Chameleons (UK), The - 07 - Looking Inwardly.lnk.mp3
├── Chameleons (UK), The - 08 - One Flesh.lnk.mp3
├── Chameleons (UK), The - 11 - In Shreds.lnk.mp3
├── Charlottes - Liar.mp3
├── City of the Sun - Sugar.mp3
├── Clap Your Hands Say Yeah - The Skin Of My Yellow Country Teeth.lnk.mp3
├── Clean, The - At The Bottom.lnk.mp3
├── Close Lobsters - Nature Thing.lnk.mp3
├── Cocteau Twins - In Our Angelhood.mp3
├── Crystal Skulls - Airport Motels.lnk.mp3
├── Crystal Stilts - 06 - Departure.lnk.mp3
├── Cub - 06 - Box of Hair.lnk.mp3
├── DIIV - Under The Sun.mp3
├── DJ Smash - 04 - Naked City.lnk.mp3
├── DJ Spooky - 01 - Ibid, Désmarches, Ibid.lnk.mp3
├── Daysleepers, The - 02 - Twilight Bloom.lnk.mp3
├── Dead Kennedys - 01- Soup Is Good Food.lnk.mp3
├── Dead Kennedys - 08 - California Uber Alles.lnk.mp3
├── Dead Kennedys - 13 - Holiday in Cambodia.lnk.mp3
├── Deafheaven - Great Mass of Color.mp3
├── Deerhunter - Desire Lines.mp3
├── Dirty Ghosts - Ropes That Way.lnk.mp3
├── Dlina Volny - Whatever Happens Next.mp3
├── Dream Shake - Ashe.lnk.mp3
├── Dream Shake - Beverly.lnk.mp3
├── Dum Dum Girls - 10 - Everybody's Out.lnk.mp3
├── Duran Duran - Girls On Film.mp3
├── Duran Duran - Last Chance On The Stairway.mp3
├── Duran Duran - Lonely In Your Nightmare (US album remix).mp3
├── Duran Duran - My Own Way.mp3
├── Duran Duran - New Religion.mp3
├── Duran Duran - Night Boat.mp3
├── Duran Duran - Planet Earth.mp3
├── Durutti Column, The - When the World.mp3
├── Eddy Current Suppression Ring - 06 - I Admit My Faults.lnk.mp3
├── Eddy Current Suppression Ring - 07 -Which Way to Go.lnk.mp3
├── Editors - 02 - An End Has A Start.lnk.mp3
├── Failure - Let it Drip.lnk.mp3
├── Fern Mayo - Moonshine Kingdom.mp3
├── Fern Mayo - New Ketamine.mp3
├── Figure Study - Wait.mp3
├── Filastine - Colony Collapse (Beats Antique Remix).mp3
├── Film School - 02 - Lectric.lnk.mp3
├── Fixx, The - Are We Ourselves.mp3
├── Fixx, The - Deeper And Deeper.mp3
├── Fixx, The - One Thing Leads To Another.mp3
├── Fixx, The - Red Skies At Night.mp3
├── Flaamingos - Walk A Wire.lnk.mp3
├── Flying Saucer Attack - My Dreaming Hill.mp3
├── Franz Ferdinand - Shopping for Blood.mp3
├── Future Islands - For Sure.mp3
├── Gallops - Pale Force.mp3
├── Gene Loves Jezebel - Shaving My Neck.lnk.mp3
├── Ghost Ease, The - Pareidolia.mp3
├── Ghost Ease, The - Supermoon (In Scorpio).mp3
├── Ghost Ease, The - XV.mp3
├── Girls Against Boys - Let Me Come Back.lnk.mp3
├── GoGo Penguin - Smarra.mp3
├── GoGoGo Airheart - Good Things.mp3
├── Gospel Gossip - 01 - Sippy Cup.lnk.mp3
├── Gun Club - 04 - She's Like Heroin To Me.lnk.mp3
├── Handsome Furs - Legal Tender.mp3
├── Harlem - 02 - Friendly Ghost.lnk.mp3
├── Heaters - Lowlife.lnk.mp3
├── Heavy Vegetable - 03 - Abducted By The Work Aliens.lnk.mp3
├── Herbaliser, The - Goldrush.mp3
├── High Violets, The - Love is blinding.lnk.mp3
├── Hives, The - Tick Tick Boom.mp3
├── Holy Hail - 02 - Luck Will Find You.lnk.mp3
├── Horrors, The - Mirrors Image.lnk.mp3
├── Horrors, The - Moving Further Away.lnk.mp3
├── House of Blondes - Do It Yourself (Landscape).lnk.mp3
├── Hovercraft - 07 - Epoxy.lnk.mp3
├── Hum.I'd Like Your Hair Long.mp3
├── I Break Horses - Faith.lnk.mp3
├── In Waves - I Saw Them.mp3
├── Interpol - 02 - Obstacle 1.lnk.mp3
├── Interpol - 04 - PDA.lnk.mp3
├── Jacuzzi Boys - Bricks Or Coconuts.lnk.mp3
├── Jet Black Factory - 01 - Real Down Ticket.lnk.mp3
├── Joe Jackson - 07 - Look Sharp!.lnk.mp3
├── Joy Formidable, The - Austere.lnk.mp3
├── Joy Formidable, The - Whirring [6m].lnk.mp3
├── Juniore - Magnifique.mp3
├── Juniore - Tout (Sinon Rien).mp3
├── Kelley Stoltz - Are You My Love.mp3
├── Kelley Stoltz - Kim Chee Taco Man.lnk.mp3
├── Killers - Smile Like You Mean It.lnk.mp3
├── Killing Joke - 08 - Eighties.lnk.mp3
├── Kills, The - Future Starts Slow.lnk.mp3
├── Kinski - 03 - Hiding Drugs In The Temple (Part2).lnk.mp3
├── Kinski - 03 - Newport.lnk.mp3
├── Kinski - 05 - Daydream Intonation.lnk.mp3
├── Kusanagi - The Infinite Bright.mp3
├── Kælan Mikla - Draumadís.mp3
├── Kælan Mikla - Hvernig kemst ég upp.mp3
├── Kælan Mikla - Næturblóm.mp3
├── Kælan Mikla - Upphaf.mp3
├── LCD Soundsystem - Call The Police.mp3
├── La Luz - Morning High.lnk.mp3
├── Ladytron - 06 - Blue Jeans 2.0.lnk.mp3
├── Lali Puna - 09 - Left-Handed.lnk.mp3
├── Lebanon Hanover - The Last Thing.mp3
├── Les Savy Fav - Hold On To Your Genre.lnk.mp3
├── Liars - Freak Out.lnk.mp3
├── Local H - Bound for the Floor.lnk.mp3
├── Longwave - 02 - Everywhere You Turn.lnk.mp3
├── Losers - This Is A War (Radio Edit).mp3
├── Love Of Diagrams - 01 - No Way Out.lnk.mp3
├── Love Of Diagrams - 01 - Static Information.lnk.mp3
├── Love Of Diagrams - 02 - Building Better Codes.lnk.mp3
├── Love Of Diagrams - 02 - The Pyramid.lnk.mp3
├── Love Of Diagrams - 03 - Lookout.lnk.mp3
├── Love Of Diagrams - 07 - Tiger Pancakes.lnk.mp3
├── LoveLikeFire - Horizon Lines.lnk.mp3
├── Lush - Gala - 01  - Sweetness And Light.mp3
├── Lush - Gala - 10  - Scarlet.mp3
├── Madlib - Young Warrior (Bobbi Humphrey).lnk.mp3
├── Madness - Night Boat To Cairo.mp3
├── Magneta Lane - Wild Gardens.lnk.mp3
├── Maps Of Norway - 02 - Manners.lnk.mp3
├── Mareotis - City Breeze.mp3
├── Marked, The - Ring Of Shadow.lnk.mp3
├── Maserati - There Will Always Be Someone Behind You.lnk.mp3
├── Messer Chups - Popcorn [live].lnk.mp3
├── Mighty Lemon Drops - Happy Head.lnk.mp3
├── Minami Deutsch - Can't Get There.mp3
├── Minami Deutsch - Sunrise, Sunset.mp3
├── Minami Deutsch - Tunnel.mp3
├── Minders, The - 357.lnk.mp3
├── Missing Persons - 04 - destination unknown.mp3
├── Missing Persons - 05 - walking in l.a.mp3
├── Missing Persons - 09 - Words.mp3
├── Modern English - I Melt With You.mp3
├── Modern Lovers - Roadrunner.lnk.mp3
├── Modulations, The - Rough Out Here.mp3
├── Moonshake - The Taboo.mp3
├── Mystery Lights, The - What Happens When You Turn The Devil Down.mp3
├── NEU - Hero.mp3
├── Ned's Atomic Dustbin - Grey Cell Green.lnk.mp3
├── Neon Indian - Slumlord.lnk.mp3
├── Nerve - To Listen Is To Love.mp3
├── New Order - d02 t06 - Cries And Whispers.lnk.mp3
├── New Sound Of Numbers - 02 - Luminous September.lnk.mp3
├── New Young Pony Club - 04 - The Bomb.lnk.mp3
├── Nico - My Heart is Empty.mp3
├── No Joy - Ghost Blonde [Live on KEXP].lnk.mp3
├── No Joy - Hare Tarot Lies.lnk.mp3
├── No Joy - Prodigy.lnk.mp3
├── No Joy - Slug Night.lnk.mp3
├── Nobody - 04 - Porpoise Song.lnk.mp3
├── Noise Figures, The - Out Of Your Mind.lnk.mp3
├── Oingo Boingo - Just Another Day.mp3
├── Oranges Band - 07 - OK Apartment.lnk.mp3
├── Orbit - Medicine.mp3
├── Overwhelming Colorfast - 05 - Every Saturday.lnk.mp3
├── PJ Harvey - Dress.mp3
├── PVT - Cold Romance.mp3
├── PVT - Nightfall.mp3
├── Palm - You Are What Eats You.mp3
├── Palomar - 06 - Beats Beat Nothing.lnk.mp3
├── Paragons - Abba.mp3
├── Parquet Courts - One Man No City.mp3
├── Pavement - 05 - Grounded.lnk.mp3
├── Prids, The - 02 - All Apart and No Fall.lnk.mp3
├── Prids, The - 03 - Let It Go.lnk.mp3
├── Prids, The - 08 - Before We Are.lnk.mp3
├── Psychedelic Furs - 01 - Pretty in Pink.mp3
├── Public Image Ltd. - 01- Public Image.lnk.mp3
├── Quantic - Time Is The Enemy.mp3
├── R.E.M. - Talk About The Passion.mp3
├── RJD2 - 10 - Chicken-Bone Circuit.lnk.mp3
├── Raincoats, The - Fairytale In The Supermarket.mp3
├── Rainer Maria - 03 - Broken Radio.lnk.mp3
├── Rainer Maria - 09 - Hell And High Water.lnk.mp3
├── Red Lorry Yellow Lorry - 04 - Monkey's On Juice.lnk.mp3
├── Red Lorry Yellow Lorry - 06 - Generation.lnk.mp3
├── Red Lorry Yellow Lorry - 12 - Walking On Your Hands.lnk.mp3
├── Red Lorry Yellow Lorry - Beating my head.mp3
├── Red Lorry Yellow Lorry - hollow eyes.mp3
├── Rentals - Song Of Remembering.mp3
├── SISU - 01 - Counting Stars.lnk.mp3
├── SISU - 05 - Sinking Feeling.lnk.mp3
├── SISU - Two Thousand Hands [Live At The Crêpe Place].lnk.mp3
├── Sacred Paws - The Conversation.mp3
├── Sacred Paws - What's So Wrong.mp3
├── Salli Lunn - The First Cause.lnk.mp3
├── Samaris - Tibrá.mp3
├── Sarge - 06 - Bedroom.lnk.mp3
├── Savage Republic - Andelusia.lnk.mp3
├── Scala - 17765744j.mp3
├── Scala - Slide.mp3
├── Schtum - Last Sad Song.lnk.mp3
├── Schtum - Low Unknown.lnk.mp3
├── Schtum - New Year Dawning.lnk.mp3
├── Sebadoh - 11 - Rebound.lnk.mp3
├── Sex Pistols - I'm Not Your Stepping Stone.mp3
├── She Wants Revenge - Sleep.mp3
├── Shellac - Copper.lnk.mp3
├── Shudder To Think - Lies About The Sky.lnk.mp3
├── Sixth June - Drowning.mp3
├── Skeletal Family - Hands on the Clock.mp3
├── Skeletal Family - Promised Land.mp3
├── Skeletal Family - She Cries Alone.mp3
├── Skeletal Family - This Time.mp3
├── Sleater Kinney - Good Things.lnk.mp3
├── Sleater-Kinney - 04 - Jumpers.lnk.mp3
├── Sleater-Kinney - Dig Me Out.lnk.mp3
├── Slowdive - Star Roving.mp3
├── Smoking Trees, The - Best Friend.mp3
├── Snail Mail - Thinning.mp3
├── Sneaker Pimps - 01 - Spin Spin Sugar (Radio Edit).lnk.mp3
├── Snowden - Black Eyes.lnk.mp3
├── Snowden - Like Bullets.lnk.mp3
├── Snowden - So Red.mp3
├── Soundcarriers, The - This Is Normal.mp3
├── Spinanes, The - 03 - Lines And Lines.lnk.mp3
├── Spindrift - Beauty.mp3
├── Spindrift- Speak To The Wind.mp3
├── Strokes, The - Reptilia.lnk.mp3
├── Strokes, The - Take It Or Leave It.lnk.mp3
├── Swans - 15 - The Great Annihilator.lnk.mp3
├── Swervedriver - Mary Winter.mp3
├── TIGER! SHIT! TIGER! TIGER! - Twins.lnk.mp3
├── Talkdemonic - 05 - Indian Angel.lnk.mp3
├── Tears Run Rings - 04 - Fall Into Light.lnk.mp3
├── Telepathe - 01 - So Fine.lnk.mp3
├── Television - 1880 Or So.mp3
├── Temples - Shelter Song.lnk.mp3
├── Ten Kens - For Posterity.lnk.mp3
├── Thee Mighty Caesars - I Self Destroy.mp3
├── Thee Oh Sees - Contraption Soul Desert.lnk.mp3
├── These New Puritans - Orion.lnk.mp3
├── Tom Waits - 11 - Down, Down, Down.lnk.mp3
├── Toy - It's Been So Long.lnk.mp3
├── Toy - To A Death Unknown.lnk.mp3
├── Triathalon - Step into the Dark.mp3
├── Tristeza - 02 - Beige Finger.lnk.mp3
├── Tristeza - 02 - Stop Grass.lnk.mp3
├── Tropic Of Cancer - A Color.mp3
├── Tropical Popsicle - The Tethers.lnk.mp3
├── Tungsten74 - Live at the Greenhouse.mp3
├── Turtle Giant - We Were Kids.lnk.mp3
├── Tvfordogs - Patient Pilgrim.lnk.mp3
├── Twin Atlas - Seem To Smile.lnk.mp3
├── Tycho - A Walk.mp3
├── Tycho - Alright.mp3
├── Tycho - Awake.mp3
├── Tycho - Coastal Brake.mp3
├── Tycho - Dive.mp3
├── Tycho - Horizon.mp3
├── Tycho - Spectre.mp3
├── Useless Eaters - Black Night Ultraviolet.lnk.mp3
├── Vampire Weekend - A-Punk.lnk.mp3
├── Vomit Launch - Hallways.lnk.mp3
├── Wand - Fire on the Mountain (I-II-III).mp3
├── Washed Out - Feel It All Around (Instrumental).lnk.mp3
├── Wavves - Tarantula.lnk.mp3
├── Wet Hair - Camouflage.lnk.mp3
├── What Made Milwaukee Famous - 08 - Selling Yourself Short.lnk.mp3
├── Winstons, The - Amen Brother.mp3
├── Wire - German Shepherds.mp3
├── Wire - In Vivo [The A List Version].mp3
├── Wire - Mannequin.mp3
├── Wire - Map Ref. 41°N 93°W.mp3
├── Wire - The 15Th.lnk.mp3
├── Wye Oak - Holy Holy.lnk.mp3
├── Yesterdays New Quintet - 16 - Kuhn's Theme.lnk.mp3
├── Yo La Tengo - 02 - Barnaby, Hardly Working.lnk.mp3
├── Yppah - Bushmills.lnk.mp3
├── Yppah - Golden Braid.lnk.mp3
├── Yppah - Gumball Machine Weekend.lnk.mp3
├── Yppah - Happy To See You.lnk.mp3
├── Yppah - Three Portraits.lnk.mp3
├── Zohar - Maroc.lnk.mp3
├── database.unignore
└── other
    ├── Jean Paul Keller - Ca S'est Arrange.mp3
    ├── afrobeat_et_al
    │   ├── Antibalas - Pay Back Africa.mp3
    │   ├── Bamboos, The - Tobago Strut.mp3
    │   ├── Black Market Brass - Big Muffler.mp3
    │   ├── Budos Band, The - T.I.B.W.F..mp3
    │   ├── Fela Kuti - Opposite People.mp3
    │   ├── Ikebe Shakedown - Supermoon.mp3
    │   ├── Nomo - Hourglass.mp3
    │   └── Nomo - The Seams.mp3
    ├── bossa_nova
    │   ├── Antônio Carlos Jobim
    │   │   ├── Antônio Carlos Jobim - Wave - 01 Wave.mp3
    │   │   ├── Antônio Carlos Jobim - Wave - 02 The Red Blouse.mp3
    │   │   ├── Antônio Carlos Jobim - Wave - 04 Batidinha.mp3
    │   │   ├── Antônio Carlos Jobim - Wave - 05 Triste.mp3
    │   │   ├── Antônio Carlos Jobim - Wave - 06 Mojave.mp3
    │   │   ├── Antônio Carlos Jobim - Wave - 08 Lamento.mp3
    │   │   ├── Antônio Carlos Jobim - Wave - 09 Antigua.mp3
    │   │   └── Antônio Carlos Jobim - Wave - 10 Captain Bacardi.mp3
    │   └── database.ignore
    ├── jazz
    │   ├── Art Blakey and Jazz Messengers - 02 - Moanin'.lnk.mp3
    │   ├── Booker Ervin - 04 - Lynn's Tune.lnk.mp3
    │   ├── Cal Tjader Quintet - A Minor Goof.lnk.mp3
    │   ├── Cannonball Adderley - 01 - Bohemia After Dark.lnk.mp3
    │   ├── Coleman Hawkins - 03 - Wrapped Tight.lnk.mp3
    │   ├── Horace Silver - 07 - Hankerin.lnk.mp3
    │   ├── John Coltrane - 01 - Giant Steps.lnk.mp3
    │   ├── Thelonious Monk - Bye-Ya.mp3
    │   ├── database.ignore
    │   └── john-coltrane
    │       ├── John Coltrane - Bakai.mp3
    │       ├── John Coltrane - Blue Train.mp3
    │       ├── John Coltrane - Chronic Blues.mp3
    │       ├── John Coltrane - Countdown.mp3
    │       ├── John Coltrane - Cousin Mary.mp3
    │       ├── John Coltrane - Lazy Bird.mp3
    │       ├── John Coltrane - Locomotion.mp3
    │       ├── John Coltrane - Moment's Notice.mp3
    │       ├── John Coltrane - Mr. P.C.mp3
    │       ├── John Coltrane - Russian Lullaby.mp3
    │       ├── John Coltrane - Spiral.mp3
    │       ├── John Coltrane - Straight Street.mp3
    │       └── John Coltrane - Syeeda's Song Flute.mp3
    ├── latin_esque
    │   ├── Buena Vista Social Club - El Cuarto de Tula.mp3
    │   ├── Colman Brothers - Sem Amor (Big Band Version).mp3
    │   ├── Richie Ray and Bobby Cruz - Chiviriquiton.mp3
    │   └── database.ignore
    ├── mardi_gras_esque
    │   ├── Clifton Chenier -- Josephine Par Se Ma Femme.mp3
    │   ├── Hot 8 Brass Band - E Flat Blues.mp3
    │   └── database.ignore
    ├── populuxe
    │   ├── Alan Hawkshaw - Bluebird.mp3
    │   ├── Ennio Morricone - Alla Luce Del Giorno.mp3
    │   ├── James Clarke - Holiday People.mp3
    │   ├── Larry Page Orchestra - Music For Night People.mp3
    │   ├── Neil Richardson - Prestige Production.mp3
    │   ├── Nelson Riddle and His Orchestra - Naked City Theme -Somewhere In The Night-.mp3
    │   ├── Otto Sieben - Curley Shirley.mp3
    │   ├── Werner Müller and his Orchestra - Hawaiian Swing
    │   │   ├── 01 - Blue Hawaii.mp3
    │   │   ├── 02 - Hawaiian War Chant.mp3
    │   │   ├── 03 - The Moon Of Manakoora.mp3
    │   │   ├── 04 - Pagan Love Song.mp3
    │   │   ├── 05 - Bali Ha'i.mp3
    │   │   ├── 06 - 'Hawaiian Eye' Theme.mp3
    │   │   ├── 07 - Sweet Leilani.mp3
    │   │   ├── 08 - Now Is The Hour.mp3
    │   │   ├── 09 - Aloha Oe.mp3
    │   │   ├── 10 - On The Beach At Waikiki.mp3
    │   │   ├── 11 - My Little Grass Shack.mp3
    │   │   └── 12 - Adventures In Paradise.mp3
    │   ├── database.ignore
    │   ├── pete_moore
    │   │   ├── Pete Moore - Asteroid.mp3
    │   │   ├── Pete Moore - Cat Walk.mp3
    │   │   ├── Pete Moore - Lazy Day.mp3
    │   │   ├── Pete Moore - Rio With Love.mp3
    │   │   ├── Pete Moore - Stakeout.mp3
    │   │   ├── Pete Moore - Terminal.mp3
    │   │   └── Pete Moore Orchestra - Turnpike Lane.mp3
    │   └── syd_dale
    │       ├── Syd Dale - London Life.mp3
    │       ├── Syd Dale Orchestra - Interstate Drive.mp3
    │       └── Syd Dale Orchestra - Sky Diver.mp3
    └── reggae_dub_ska
        ├── Abyssinians , The - Declaration Of Rights.mp3
        ├── Big Youth - Dreader Dan Dread.mp3
        ├── Billy Joe Morgan - Stop Them.mp3
        ├── Burning Spear - I And I Survive.mp3
        ├── Dub Trio - Mortar Dub.mp3
        ├── Gregory Isaacs - Leggo Beast.mp3
        ├── King Tubby - King Tubby Meets the Rockers Uptown.mp3
        ├── Lion Youth - Rat A Cut Bottle [12 inch].mp3
        ├── Naggo Morris and U Roy - Say You.mp3
        ├── Skatalites - Nimblefoot Ska.lnk.mp3
        ├── Slits, The - Man Next Door - version - 1980.mp3
        └── Slits, The - Spend Spend Spend.mp3

13 directories, 430 files
```
